﻿using crud_alumnos.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace crud_alumnos.Controllers
{
    public class AlumnoController : Controller
    {
        // GET: Alumno
        public ActionResult Index()
        {
            try
            {
                using (var db = new AlumnosContext())
                {
                    List<alumno> lista = db.alumno.ToList();
                    return View(lista);
                }
            }
            catch (Exception)
            {

                throw;
            }
        }
        public ActionResult Agregar()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Agregar(alumno a)
        {
            if (!ModelState.IsValid)
                return View();
            try
            {
                using (var db = new AlumnosContext())
                {
                    a.fecharegistro = DateTime.Now;
                    db.alumno.Add(a);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", "Error al registrar Alumno ");
                return View();
            }
        }
        public ActionResult Editar(int id)
        {
            try
            {
                using (var db = new AlumnosContext())
                {
                    alumno al = db.alumno.Find(id);
                    return View(al);
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Editar(alumno a)
        {
            try
            {
                if (!ModelState.IsValid)
                    return View();
                using (var db = new AlumnosContext())
                {
                    alumno al = db.alumno.Find(a.id);
                    al.nombres = a.nombres;
                    al.apellidos = a.apellidos;
                    al.edad = a.edad;
                    al.sexo = a.sexo;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        public ActionResult DetallesAlumno(int id)
        {
            try
            {

                using (var db = new AlumnosContext())
                {
                    alumno al = db.alumno.Find(id);
                    return View(al);
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        public ActionResult EliminarAlumno(int id)
        {
            try
            {
                using (var db = new AlumnosContext())
                {
                    alumno al = db.alumno.Find(id);
                    db.alumno.Remove(al);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}