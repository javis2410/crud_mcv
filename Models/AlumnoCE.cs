﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace crud_alumnos.Models
{
    public class AlumnoCE
    {
        [Required]
        [Display(Name ="Ingrese Nombres")]
        public string nombres { get; set; }
        [Required]
        [Display(Name = "Ingrese Apellidos")]
        public string apellidos { get; set; }
        [Required]
        [Display(Name = "Edad del Alumno")]
        public int edad { get; set; }
        [Required]
        [Display(Name = "Sexo del Alumno")]
        public string sexo { get; set; }
    }
    [MetadataType(typeof(AlumnoCE))]
    public partial class alumno
    {
        public string NombreCompleto { get { return nombres + " " + apellidos; } }
    }
}